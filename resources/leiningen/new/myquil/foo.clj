(defproject sketch "1.0"
  :description "My template sketch."
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [quil "2.7.1" :exclusions [org.clojure/clojure]]
                 [org.apache.commons/commons-math3 "3.6.1"]
                 [incanter "1.9.3"]]
  :jvm-opts ["-Xms1100m" "-Xmx1100M" "-server"]
  :source-paths ["src/clj"]
  :java-source-paths ["src/java"]
  :aot [sketch.dynamic])
