(ns sketch.runcore
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [clojure.string :as string]
            [clojure.tools.cli :refer [parse-opts]]
            [sketch.dynamic :as dynamic])
  (:gen-class))

(def cli-options
  ;; An option with a required argument
  [["-w" "--width WIDTH" "Image width"
    :default 600
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 10800) "Must be a number between 0 and 65536"]]
  ["-g" "--height HEIGHT" "Image height"
    :default 1000
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 10800) "Must be a number between 0 and 65536"]]
  ["-s" "--seed SEED" "Random seed"
    :default 1000
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
  ["-t" "--times TIMES" "Number of times to repeat the process"
    :default 1000
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
   ;; A non-idempotent option (:default is applied first)
   ;; A boolean option defaulting to nil
   ["-h" "--help"]])

(defn usage [options-summary]
  (->> ["Run the generative program"
        ""
        "Usage: runcore [options] action"
        ""
        "Options:"
        options-summary
        "Please refer to the manual page for more information."]
       (string/join \newline)))

(defn error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (string/join \newline errors)))

(defn validate-args
  "Validate command line arguments. Either return a map indicating the program
  should exit (with a error message, and optional ok status), or a map
  indicating the action the program should take and the options provided."
  [args]
  (let [{:keys [options arguments errors summary]} (parse-opts args cli-options)]
    (cond
      (:help options) ; help => exit OK with usage summary
      {:exit-message (usage summary) :ok? true}
      errors ; errors => exit with description of errors
      {:exit-message (error-msg errors)}
      ;; custom validation on arguments
      (and (= 1 (count arguments))
           (#{"start" "stop" "status"} (first arguments)))
      {:action (first arguments) :options options}
      :else ; failed custom validation => exit with usage summary
      {:exit-message (usage summary)})))

(defn exit [status msg]
  (println msg)
  (System/exit status))


(defn -main [& args]
  (let [{:keys [width height seed times options exit-message ok?]} (validate-args args)]
    (if exit-message
      (exit (if ok? 0 1) exit-message)
      (dotimes [n times]
        (q/sketch
          :title "{{name}} - large"
          :setup (partial dynamic/setup seed n)
          :update dynamic/update-state
          :draw dynamic/draw-state-and-save
          :middleware [m/fun-mode]
          :size [width height]
          :features [:exit-on-close])))))
