(defproject {{name}} "1.0"
  :description "{{name}}"
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/tools.cli "0.4.1"]
                 [quil "2.7.1" :exclusions [org.clojure/clojure]]
                 [org.apache.commons/commons-math3 "3.6.1"]
                 [incanter "1.9.3"]]
  :jvm-opts ["-Xms5000m" "-Xmx5000M" "-server"]
  :aot [sketch.dynamic sketch.runcore])
