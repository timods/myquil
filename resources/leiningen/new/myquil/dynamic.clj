(ns sketch.dynamic
  (:require [quil.core :refer :all])
  (:use [incanter.core :only [$=]])
  (:use [clojure.math.combinatorics :only [combinations cartesian-product]])
  (:use [clojure.pprint])
  (:use [clojure.java.shell :only [sh]])
  (:use [clojure.string :only [join split]])

  (:use [clojure.set :only [union difference]])
  (:import [org.apache.commons.math3.distribution ParetoDistribution])
  (:import [processing.core PShape PGraphics]))

(defn save-and-compress [state]
  (let [frame (frame-count)
        filename (str (:outdir state) "/frame-" frame "-full.png")
        thumbnail (str (:outdir state) "/frame-" frame "-1000.tif")]
    (save filename)
    (sh "convert" "-LZW" filename filename)
    (sh "convert" "-scale" "1000x1000" filename thumbnail)))

(defn h
  ([] (h 1.0))
  ([value] (* (height) value)))

(defn w
  ([] (w 1.0))
  ([value] (* (width) value)))

(defn setup [seed trial]
  (random-seed seed)
  ;; Output directory is the start time hyphen random seed hyphen trial number
  (let [outdir (join "" ["output/" (.format (java.text.SimpleDateFormat. "MM-dd-yyyy_HH.mm.ss") (new java.util.Date)) "-" seed "-" trial])]
    (.mkdir (java.io.File. outdir))
    {:x 100
     :y 100
     :trial trial
		 :outdir outdir
     :width 400
     :height 400}
    )
  )

(defn update-state [state]
  "given the old state, return the new state"
  (update-in state [:x] inc))


(defn draw-state [state]
  (no-loop)
  (color-mode :hsb 360 100 100 1.0)
  (background 220 49 66)
  (rect (:x state) (:y state)
        (:width state) (:height state))
  (save-and-compress state))

(defn draw-state-and-save [state]
  (draw-state state)
  (save-frame (join "" [(:outdir state) "/frame-####.png"])))
