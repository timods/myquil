(ns sketch.core
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [sketch.dynamic :as dynamic])
  (:gen-class))

(q/defsketch example
             :title "Sketch"
             :setup (partial dynamic/setup (int (Math/floor (rand-int 65535))) 0)
             :update dynamic/update-state
             :draw dynamic/draw-state
             :middleware [m/fun-mode]
             :size [900 900])

(defn refresh []
  (use :reload 'sketch.dynamic)
  (.loop example))
