(ns leiningen.new.myquil
  (:require [leiningen.new.templates :refer [renderer name-to-path ->files]]
            [leiningen.core.main :as main]))

(def render (renderer "myquil"))

(defn myquil
  "FIXME: write documentation"
  [name]
  (let [data {:name name
              :sanitized (name-to-path name)}]
    (main/info "Generating fresh 'lein new' myquil project.")
    (->files data
             ["project.clj" (render "project.clj" data)]
             ["src/sketch/dynamic.clj" (render "dynamic.clj" data)]
             ["src/sketch/runcore.clj" (render "runcore.clj" data)]
             ["src/sketch/core.clj" (render "core.clj" data)]
             )))
